<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Home\HomeController@index');
Route::get('/{params}/user', 'Profil\UserProfileController@index');
Route::get('/login', 'Auth\AuthenticationController@index');
Route::get('/logout', 'Auth\AuthenticationController@logout');
Route::get('/registration', 'Auth\AuthenticationController@showRegister');
Route::get('/{params}/detail', 'Home\HomeController@detailPost');
Route::get('/{id}/comment', 'Home\HomeController@showCommentPage');
Route::get('/search/{params}', 'Home\HomeController@search');
Route::get('/search', 'Home\HomeController@search');
Route::post('/follow', 'Home\HomeController@doFollowOrUnfollow');
Route::get('/bookmark', 'Home\HomeController@getBookmark');

Route::post('/register', 'Auth\AuthenticationController@doRegister');
Route::post('/login', 'Auth\AuthenticationController@doLogin');
Route::post('/upload', 'Home\HomeController@uploadSomething');
Route::post('/like', 'Home\HomeController@likePost');
Route::post('/bookmark', 'Home\HomeController@bookmarkPost');
Route::post('/addFriend', 'Home\HomeController@addFriend');
Route::post('/comment', 'Home\HomeController@doComment');
Route::post('/edit-comment', 'Home\HomeController@editComment');
Route::post('/edit-caption', 'Home\HomeController@editCaption');
Route::delete('/delete', 'Home\HomeController@doDelete');

Route::get('{params}/edit', 'Profil\UserProfileController@editProfile');
Route::get('{params}/followers', 'Profil\UserProfileController@showFollowers');
Route::get('{params}/following', 'Profil\UserProfileController@showFollowing');


Route::put('edit-profile', 'Profil\UserProfileController@doEditProfile');
