<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'user_id';

    private $iduser;
    private $fullname;
    private $username;
    private $email;
    private $password; 

    public function __construct($iduser = null, $fullname = null, $username = null,$email = null,$password = null)
    {
        $this->iduser = $iduser;
        $this->fullname = $fullname;
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function setIdUser($iduser)
    {
        $this->iduser = $iduser;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getIdUser()
    {
        return $this->iduser;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getFullname()
    {
        return $this->fullname;
    }
}
