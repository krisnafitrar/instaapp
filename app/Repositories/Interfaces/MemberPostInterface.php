<?php

namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;

interface MemberPostInterface
{
    public function getPostLists();
    public function getPostDetail($id);
    public function makePost(Request $request);
}
