<?php

namespace App\Repositories\Interfaces;

use App\Models\UserModel;

interface AuthenticationInterface
{
    public function doLogin(UserModel $model);
    public function saveUser(UserModel $model);
}
