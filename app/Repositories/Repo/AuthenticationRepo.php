<?php

namespace App\Repositories\Repo;

use App\Models\UserModel;
use App\Repositories\Interfaces\AuthenticationInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AuthenticationRepo implements AuthenticationInterface
{
    
    public function __construct()
    {
        
    }

    public function doLogin(UserModel $model)
    {
        $usernameOrEmail = $model->getUsername();
        $password = $model->getPassword();

        //define ip address
        $ip_address = $_SERVER['REMOTE_ADDR'];
        
        $userAttempts = Session::get($ip_address . 'failed_attempts');

        $loginAttemptData = [
            'usernameoremail' => $usernameOrEmail,
            'ip_address' => $ip_address,
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
            'is_success' => 1
        ];

        if(isset($userAttempts)){
            if(
                $userAttempts['ip_address'] == $ip_address and 
                $userAttempts['total'] >= 7
            ){
                if (!isset($attempts['waiting_time'])) {
                    Session::put($ip_address . '_' . 'failed_attempts', [
                        'ip$ip_address' => $ip_address,
                        'total' => $userAttempts['total'],
                        'waiting_time' => time() + 60
                    ]);
                }

                if (Session::get($ip_address . '_' . 'failed_attempts')['waiting_time'] - time() <= 0) {
                    Session::forget($ip_address . '_' . 'failed_attempts');
                } else {
                    return redirect('/login')->with('error', 'Terlalu banyak percobaan masuk, Silahkan coba lagi dalam ' . (Session::get($ip_address . '_' . 'failed_attempts')['waiting_time'] - time()) . ' detik');
                }

            }
        }

        //jika tidak spam maka query ke db untuk mengambil data user yang sesuai emailnya
        $user = DB::table('users')
                            ->where('email', $usernameOrEmail)
                            ->orWhere('username', $usernameOrEmail)
                            ->first();

        //user tidak ditemukan return exception
        if (!$user) {
            return redirect('/login')->with('error', 'User tidak ditemukan!');
        }

        if ($user->is_active == 0) {
            return redirect('/login')->with('error', 'User tidak aktif');
        }

                //password salah & insert failed flag login attempt
        if (!Hash::check($password, $user->password)) {
            $this->createFailedLoginAttempt($loginAttemptData);
            if (!Session::get($ip_address . '_' . 'failed_attempts')) {
                Session::put($ip_address . '_' . 'failed_attempts', [
                            'ip_address' => $ip_address,
                            'total' => 1,
                            'time' => time()
                        ]);
            } else {
                //jika salah memasukkan password kurang dari satu menit maka dihitung spam
                if ((time() - Session::get($ip_address . '_' . 'failed_attempts')['time']) <= 60) {
                    Session::put($ip_address . '_' . 'failed_attempts', [
                                'ip_address' => $ip_address,
                                'total' => Session::get($ip_address . '_' . 'failed_attempts')['total'] + 1,
                                'time' => time()
                            ]);
                } else {
                    //reset failed attempts karena percobaan lebih dari 1 menit (dianggap tidak spam)
                    Session::put($ip_address . '_' . 'failed_attempts', [
                                'ip_address' => $ip_address,
                                'total' => 1,
                                'time' => time()
                            ]);
                        }
                }
            return redirect('/login')->with('error', 'Password yang anda masukkan salah!');
        }

        //sukses skenario
        if (Session::get($ip_address . '_' . 'failed_attempts')) {
            Session::flush();
        }

        $token = random_str();
        $token_expired = time() + (60 * 60 * 2);

        $loginAttemptData['token'] = $token;
        $loginAttemptData['token_expired'] = $token_expired;

        $this->createSuccessLoginAttempt($loginAttemptData);

        //set session
        Session::put('token', $token);
        Session::put('user_id', $user->user_id);

        return redirect('/');
    }

    public function createFailedLoginAttempt($data)
    {
        $data['is_success'] = 0;

        DB::table('login_attempts')->insert($data);
    }

    public function createSuccessLoginAttempt($data)
    {
        $data['is_success'] = 1;
        DB::table('login_attempts')->insert($data);
    }


    public function saveUser(UserModel $model)
    {
        //create member
        $idmember = bin2hex(random_bytes(8));

        $arr_member = array();

        $arr_member = [
            'member_id' => $idmember,
            'member_name' => $model->getFullname()
        ];

        $arr_users = [
            'user_id' => $model->getIdUser(),
            'member_id' => $idmember,
            'fullname' => $model->getFullname(),
            'username' => $model->getUsername(),
            'email' => $model->getEmail(),
            'password' => Hash::make($model->getPassword()),
            'is_loggedin' => 0,
            'user_payloads' => json_encode(array(
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'user_agent' => $_SERVER['HTTP_USER_AGENT']
            )),
            'is_active' => 1,
        ];

        $user = DB::table('users')
                                ->where('username', $model->getEmail())
                                ->orWhere('email', $model->getEmail())
                                ->first();

        if($user){
            return redirect('registration')->with('error', 'Username atau password sudah digunakan!');
        }
        
        try{
            DB::beginTransaction();

            DB::table('members')->insert($arr_member);

            DB::table('users')->insert($arr_users);

            $ok = DB::commit();

            return redirect('registration')->with('success', 'Berhasil membuat akun');
        }catch(\Exception $e){
            DB::rollBack();
            throw $e;
        }
    }


}
