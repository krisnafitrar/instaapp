<?php

namespace App\Repositories\Repo;

use App\Repositories\Interfaces\MemberPostInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MemberPostRepo implements MemberPostInterface
{

    public function __construct()
    {
    }

    public function getPostLists()
    {
        $data = DB::table('member_posts')
            ->join('post_images', 'member_posts.post_id', '=', 'post_images.post_id')
            ->join('users', 'member_posts.member_id', '=', 'users.member_id')
            ->where('member_posts.member_id', getUserDetails()->member_id)
            ->orWhereIn('member_posts.member_id', function($query){
                $query->select('member_id')->from('member_followers')->where('follower_id', getUserDetails()->member_id);
            })
            ->select('member_posts.*', 'post_images.*', 'users.username', 'users.photo')
            ->orderBy('member_posts.time', 'desc')
            ->get()
            ->groupBy('member_posts.post_id');

        return $this->collectPostById($data);
    }

    public function getPostDetail($id)
    {
        $data = DB::table('member_posts')
        ->join('post_images', 'member_posts.post_id', '=', 'post_images.post_id')
        ->join('users', 'member_posts.member_id', '=', 'users.member_id')
        ->where('member_posts.post_id', $id)
        ->select('member_posts.*', 'post_images.*', 'users.username', 'users.photo')
        ->get()
        ->groupBy('member_posts.post_id');

        return $this->collectPostById($data);
        
    }

    public function getMyPostLists($params = null)
    {
        if(!empty($params)){
            $where = 'users.username';
            $pass = $params;
        }else{
            $where = 'member_posts.member_id';
            $pass = getUserDetails()->member_id;
        }

        $data = DB::table('member_posts')
        ->join('post_images', 'member_posts.post_id', '=', 'post_images.post_id')
        ->join('users', 'member_posts.member_id', '=', 'users.member_id')
        ->where($where , $pass )
        ->select('member_posts.*', 'post_images.*', 'users.username', 'users.photo')
        ->orderBy('member_posts.time', 'desc')
        ->get()
        ->groupBy('member_posts.post_id');

        return $this->collectPostById($data);
    }

    public function makePost(Request $request)
    {
    }

    public function collectPostById($data)
    {
        $images = array();
        $postId = null;
        $index = -1;
        $postLists = array();

        foreach ($data as $k => $v) {
            foreach ($v as $key => $value) {

                if ($postId != $value->post_id) {
                    $images = [];
                }

                array_push($images, $value->image);

                if ($postId != $value->post_id) {
                    $postLists[] = array(
                        'post_id' => $value->post_id,
                        'member_id' => $value->member_id,
                        'username' => $value->username,
                        'total_likes' => $value->total_likes,
                        'total_comments' => $value->total_comments,
                        'photo' => $value->photo,
                        'caption' => $value->caption,
                        'ip_address' => $value->ip_address,
                        'time' => $value->time,
                        'images' => $images
                    );

                    $index++;
                } else {
                    $postLists[$index]['images'] =  $images;
                }

                $postId = $value->post_id;
            }
        }


        return $postLists;
    }
}
