<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MemberPostProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Interfaces\MemberPostInterface',
        'App\Repositories\Repo\MemberPostRepo');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
