<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

function getFlashMessage()
{
    if (session('success')) {
        echo '<div class="alert alert-success alert-dismissible fade show" role="alert">' . session('success') . '               
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>';
        return 'success';
    } elseif (session('warning')) {
        echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">' . session('warning') . '               
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>';
        return 'warning';
    } elseif (session('error')) {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">' . session('error') . '               
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>';
        return 'error';
    }   
}

function random_str($lenght = 16)
{
    return substr(str_shuffle(str_repeat($x='01234567899abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($lenght / strlen($x)))),1,$lenght);
}

function isTokenValid()
{
    $token = Session::get('token');
    $ip_address = $_SERVER['REMOTE_ADDR'];
    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $login_attempt = DB::table('login_attempts')->where('token', $token)->first();

    if(!$login_attempt){
        return false;
    }

    if(time() > $login_attempt->token_expired){
        return false;
    }

    if($login_attempt->ip_address != $ip_address 
            && $login_attempt->user_agent != $user_agent){
        return false;
    }

    return true;
}

function getUserDetails($params = null)
{
    $user = null;

    if(isTokenValid()){
        if(empty($params)){
            $user = DB::table('users')
            ->join('members', 'users.member_id', 'members.member_id')
            ->where('users.user_id', Session::get('user_id'))
            ->first();
        }else{
            $user = DB::table('users')
            ->join('members', 'users.member_id', 'members.member_id')
            ->where('users.username', $params)
            ->first();
        }
    }

    return $user;
}

function getComments()
{
    $data = DB::table('post_comments')
                ->join('members', 'post_comments.member_id', '=', 'members.member_id')
                ->join('users', 'post_comments.member_id', '=', 'users.member_id')
                ->orderBy('post_comments.time', 'desc')
                ->get();

    $newData = array();
    $postId = null;

    foreach ($data as $d) {
        $postId = $d->post_id;

        if($postId == $d->post_id){
            $newData[$d->post_id][] = array(
                'comment_id' => $d->comment_id,
                'post_id' => $d->post_id,
                'member_id' => $d->member_id,
                'comment' => $d->comment,
                'time' => $d->time,
                'username' => $d->username,
                'photo' => $d->photo
            );
        }
    }

    return $newData;
}

function getDefinitionTime($params)
{
    //hitung waktu
    $time = $params;
    $countRange = time() - $time;
    $realTime = null;

    if($countRange < 60){
        $realTime = $countRange . ' detik yang lalu';   
    }else{
        $a = ($countRange / 60); 

        if($a >= 1 && $a < 60){
            $realTime = floor($a) . ' menit yang lalu';
        }else if($a > 60 && floor(($a / 60)) < 24){
            $realTime = floor(($a / 60)) . ' jam yang lalu';
        }else{
            $realTime = floor(($a / (60 * 24))) . ' hari yang lalu';
        }
    }

    return $realTime;
}

function getLikes()
{

    $data = DB::table('post_likes')
            ->get()
            ->groupBy('post_id');

    $postId = null;

    foreach ($data as $value) {
        $arr = array();
        foreach ($value as $val) {
            $postId = $val->post_id;
            if($postId == $val->post_id){
                array_push($arr,$val->like_by);
                $newData[$val->post_id] = $arr;
            }
        }
    }

    return $newData;

}

function getBookmarks()
{
    $data = DB::table('member_bookmarks')
            ->get()
            ->groupBy('post_id');

    $postId = null;

    foreach ($data as $value) {
        $arr = array();
        foreach ($value as $val) {
            $postId = $val->post_id;
            if($postId == $val->post_id){
                array_push($arr,$val->member_id);
                $newData[$val->post_id] = $arr;
            }
        }
    }

    return $newData;

}
