<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\UserModel;
use App\Repositories\Repo\AuthenticationRepo as RepoAuthenticationRepo;
use AuthenticationRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AuthenticationController extends Controller
{
    private $repository;

    public function __construct(RepoAuthenticationRepo $repo)
    {
        $this->middleware(function ($request, $next){
            if($_SERVER['REQUEST_URI'] != '/logout'){
                if(isTokenValid()){
                    return redirect('/');
                }
            }
            return $next($request);
        });

        $this->repository = $repo;
    }

    public function index()
    {
        $data['title'] = 'Masuk InstaApp';

        return view('auth/index', $data);
    }

    public function showRegister()
    {
        $data['title'] = 'Registrasi InstaApp';

        return view('auth/registration', $data);
    }

    public function doLogin(Request $request)
    {
        $this->validate($request, array(
            'identity' => 'string|required',
            'password' => 'string|required'
        ));

        $model = new UserModel();
        $model->setUsername($request->input('identity'));
        $model->setPassword($request->input('password'));

        return $this->repository->doLogin($model);
    }

    public function doRegister(Request $request)
    {
        $this->validate($request, [
            'fullname' => 'string|required',
            'username' => 'string|required',
            'email' => 'string|email',
            'password' => 'string|required',
            'password_confirm' => 'string|required|same:password'
        ]);

        $model = new UserModel(bin2hex(random_bytes(8)),
                                $request->input('fullname'),$request->input('username'),
                                $request->input('email'),
                                $request->input('password'));
        
        return $this->repository->saveUser($model);
    }

    public function logout()
    {
        Session::flush();
        return redirect('/login')->with('success', 'Berhasil logout!');
    }
}
