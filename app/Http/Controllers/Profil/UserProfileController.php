<?php

namespace App\Http\Controllers\Profil;

use App\Http\Controllers\Controller;
use App\Repositories\Repo\MemberPostRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserProfileController extends Controller
{
    private $repository;

    public function __construct(MemberPostRepo $repo)
    {
        $this->middleware(function ($request, $next) {
            if (!isTokenValid()) {
                return redirect('login');
            }

            parent::__construct();

            return $next($request);
        });

        $this->repository = $repo;
    }

    public function index($params)
    {
        $data['title'] = 'InstaApp | Profil';
        $data['user'] = $this->userDetails;
        $data['profile'] = getUserDetails($params);
        $data['posts'] = $this->repository->getMyPostLists($params);
        $data['followers'] = DB::table('member_followers')->where('member_id', $data['profile']->member_id)->get();
        $data['following'] = DB::table('member_followers')->where('follower_id', $data['profile']->member_id)->get();

        $array = array();

        foreach ($data['followers'] as $val) {
            array_push($array, $val->follower_id); 
        }

        $data['followers'] = $array;


        return view('profile/index', $data);
    }

    public function editProfile()
    {
        $data['title'] = 'InstaApp | Edit Profil';
        $data['user'] = $this->userDetails;

        return view('profile/edit', $data);
    }


    public function doEditProfile(Request $request)
    {
        $fullname = $request->input('fullname');
        $username = $request->input('username');
        $email = $request->input('email');
        $website = $request->input('website');
        $biography = $request->input('biography');
        $gender = $request->input('gender');
        $birthdate = $request->input('birthdate');
        $telp_number = $request->input('telp_number');
        $old_password = $request->input('old_password');
        $new_password = $request->input('new_password');
        $repeat_password = $request->input('repeat_password');

        $act = $request->input('act');
        $key = $this->userDetails->user_id;
        $member_id = $this->userDetails->member_id;

        $profil_data = array(
            'fullname' => $fullname,
            'username' => $username,
            'email' => $email
        );

        if($act == "profile"){
            $this->validate($request,[
                'fullname' => 'required|string',
                'username' => 'required|string',
                'email' => 'required|email'
            ]);

    
            $get = DB::table('users')->where('username', $username)->first();

            if($get){
                if($get->user_id != $key){
                    return redirect($this->userDetails->username . '/edit')->with('error', 'Username telah digunakan');
                }
            }

            $getMore = DB::table('users')->where('email', $email)->first();

            if($getMore){
                if($getMore->user_id != $key){
                    return redirect($this->userDetails->username . '/edit')->with('error', 'Email telah digunakan');
                }
            }

            try {
                DB::beginTransaction();
                
                if($request->hasFile('image')){
                    $file = $request->file('image');
                    $name = random_str('5');
                    $extension = $file->getClientOriginalExtension();
                    $newName = $name . '.' . $extension;
    
                    $profil_data['photo'] = $newName;

                    Storage::putFileAs('public/users', $file, $newName);
                }
                
                
                $ok = DB::table('users')->where('user_id',$key)->update($profil_data);
                $ok = DB::table('members')->where('member_id', $member_id)->update(['member_name' => $fullname, 'website' => $website,'biography' => $biography,]);
                DB::commit();

                return redirect($this->userDetails->username . '/edit')->with('success', 'Berhasil merubah profil');

            } catch (\Throwable $th) {
                DB::rollBack();
                throw $th;
            }    

        }elseif ($act == "private") {
            $this->validate($request, array('fullname' => 'required|string'));
            $data = array(
                'member_name' => $fullname,
                'gender' => $gender,
                'birthdate' => $birthdate,
                'telp_number' => $telp_number
            );

            $ok = DB::table('members')->where('member_id', $member_id)->update($data);

            if($ok)
               return redirect($this->userDetails->username . '/edit')->with('success', 'Berhasil merubah data pribadi');
            else
                return redirect($this->userDetails->username . '/edit')->with('error', 'Gagal merubah data pribadi');


        }elseif ($act == "password") {
            if(empty($old_password)){
                return redirect($this->userDetails->username . '/edit')->with('error', 'Password tidak boleh kosong');
            }            
            
            if(empty($new_password)){
                return redirect($this->userDetails->username . '/edit')->with('error', 'Password tidak boleh kosong');
            }            
            
            if(empty($repeat_password)){
                return redirect($this->userDetails->username . '/edit')->with('error', 'Password tidak boleh kosong');
            }            

            $current_password = $this->userDetails->password;

            if(!Hash::check($old_password, $current_password)){
                return redirect($this->userDetails->username . '/edit')->with('error', 'Password lama salah');
            }

            if($new_password != $repeat_password){
                return redirect($this->userDetails->username . '/edit')->with('error', 'Password tidak cocok');
            }

            if(Hash::check($new_password, $current_password)){
                return redirect($this->userDetails->username . '/edit')->with('error', 'Password tidak boleh sama dengan password saat ini');
            }

            if(strlen($new_password) < 8){
                return redirect($this->userDetails->username . '/edit')->with('error', 'Password panjang minimal 8 karakter');
            }
            

            //lolos
            $ok = DB::table('users')->where('user_id', $this->userDetails->user_id)->update(['password' => Hash::make($new_password)]);

            if($ok)
                return redirect($this->userDetails->username . '/edit')->with('success', 'Berhasil merubah password');
           else
             return redirect($this->userDetails->username . '/edit')->with('error', 'Gagal merubah password');

        }


    }

    public function showFollowers($params)
    {
        $data['title'] = 'InstaApp | Followers';
        $data['user'] = $this->userDetails;
        $data['profile'] = getUserDetails($params);
        $data['followers'] = DB::table('member_followers')
                            ->join('users', 'member_followers.follower_id', '=', 'users.member_id')
                            ->where('member_followers.member_id', $data['profile']->member_id)
                            ->select('users.username', 'users.fullname', 'users.photo', 'users.member_id')
                            ->get();
        $data['following'] = DB::table('member_followers')->where('follower_id', $data['profile']->member_id)->get();
        
        $array = array();

        foreach ($data['following'] as $val) {
            array_push($array, $val->member_id); 
        }

        $data['following'] = $array;

        return view('profile/followers', $data);
    }
}
