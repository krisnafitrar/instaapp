<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Repositories\Repo\MemberPostRepo;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    private $repository;

    public function __construct(MemberPostRepo $repo)
    {
        $this->middleware(function ($request, $next) {
            if (!isTokenValid()) {
                return redirect('login');
            }

            parent::__construct();

            return $next($request);
        });

        $this->repository = $repo;
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'InstaApp';
        $data['user'] = $this->userDetails;
        $data['active'] = 'home';
        $data['posts'] = $this->repository->getPostLists();

        // dd($data['posts']);

        return view('home.index', $data);
    }

    public function showCommentPage($post_id)
    {
    }

    public function uploadSomething(Request $request)
    {
        try {
            $caption = $request->input('caption');

            DB::beginTransaction();

            $postId = random_str(8);

            $arrData = array(
                'post_id' => $postId,
                'member_id' => $this->userDetails->member_id,
                'caption' => $caption,
                'total_likes' => 0,
                'total_comments' => 0,
                'time' => time(),
                'ip_address' => $_SERVER['REMOTE_ADDR']
            );

            if ($request->hasFile('images')) {

                //insert post
                DB::table('member_posts')->insert($arrData);

                $files = $request->file('images');

                foreach ($files as $file) {
                    $name = random_str('5');
                    $extension = $file->getClientOriginalExtension();
                    $newName = $name . '.' . $extension;
                    $size = $file->getSize();

                    Storage::putFileAs('public/posts', $file, $newName);

                    $dataImages = array(
                        'post_id' => $postId,
                        'image' => $newName,
                        'image_size' => $size,
                        'image_type' => $extension,
                        'path' => 'storage/posts/' . $newName
                    );

                    DB::table('post_images')->insert($dataImages);
                }

                DB::commit();

                echo json_encode(array(
                    'response' => 200,
                    'message' => 'Berhasil mengupload postingan'
                ));
            } else {
                return redirect('/')->with('error', 'Tidak ada file yang diupload!');
            }
        } catch (Exception $e) {
            DB::rollBack();
            echo json_encode(
                array(
                    'response' => 501,
                    'message' => $e->getMessage()
                )
            );
            // throw $e;
        }
    }

    public function likePost(Request $request)
    {
        $member_id = $request->input('member_id');
        $post_id = $request->input('post_id');

        $isLikes = DB::table('post_likes')->where('post_id', $post_id)->where('like_by', $member_id)->first();
        $totalLikes = DB::table('member_posts')->select('total_likes')->where('post_id', $post_id)->first();
        $act = "add";

        if ($isLikes) {
            $ok = DB::table('post_likes')->delete($isLikes->id);
            DB::table('member_posts')
                ->where('post_id', $post_id)
                ->update(['total_likes' => $totalLikes->total_likes - 1]);
            $act = "del";
        } else {
            $ok = DB::table('post_likes')->insert(array(
                'post_id' => $post_id,
                'like_by' => $member_id,
                'ip_address' => $_SERVER['REMOTE_ADDR']
            ));
            DB::table('member_posts')
                ->where('post_id', $post_id)
                ->update(['total_likes' => $totalLikes->total_likes + 1]);
            $act = "add";
        }

        echo json_encode(array(
            'response' => 200,
            'message' => 'Berhasil ' . $act . ' like',
            'act' => $act
        ));
    }

    public function bookmarkPost(Request $request)
    {
        $member_id = $request->input('member_id');
        $post_id = $request->input('post_id');

        $isBookmark = DB::table('member_bookmarks')->where('post_id', $post_id)->where('member_id', $member_id)->first();
        // $totalBookmark = DB::table('member_posts')->select('total_Bookmark')->where('post_id', $post_id)->first();
        $act = "add";

        if ($isBookmark) {
            $ok = DB::table('member_bookmarks')->delete($isBookmark->id);
            $act = "del";
        } else {
            $ok = DB::table('member_bookmarks')->insert(array(
                'post_id' => $post_id,
                'member_id' => $member_id
            ));
            $act = "add";
        }

        echo json_encode(array(
            'response' => 200,
            'message' => 'Berhasil ' . $act . ' like',
            'act' => $act
        ));
    }

    public function getBookmark()
    {
        $data['title'] = 'InstaApp | Bookmarks';
        $data['user'] = $this->userDetails;
        $data['active'] = 'home';
        $data['bookmarks'] = DB::table('member_bookmarks')->join('member_posts', 'member_bookmarks.post_id', 'member_posts.post_id')->join('post_images', 'member_bookmarks.post_id', '=', 'post_images.post_id')->where('member_bookmarks.member_id', $this->userDetails->member_id)->get();
        $array = array();
        $postId = null;

        foreach ($data['bookmarks'] as $value) {

            if ($postId != $value->post_id) {
                $array[] = $value;
            }

            $postId = $value->post_id;
        }

        $data['bookmarks'] = $array;

        return view('bookmark/index', $data);
    }

    public function search()
    {
        $data['title'] = 'InstaApp | Search';
        $data['user'] = $this->userDetails;
        $data['active'] = 'search';
        $data['member_followers'] = DB::table('member_followers')->where('follower_id', $this->userDetails->member_id)->get();

        $array = array();

        foreach ($data['member_followers'] as $val) {
            array_push($array, $val->member_id); 
        }

        $data['member_followers'] = $array;


        $keyword = isset($_GET['keyword']) ? $_GET['keyword'] : null;

        if (!empty($keyword)) {
            $data['member'] = DB::table('users')->join('members', 'users.member_id', '=', 'members.member_id')->where('users.username', 'like', '%' . $keyword . '%')->get();
        }

        return view('home/search', $data);
    }

    public function doComment(Request $request)
    {
        $member_id = $this->userDetails->member_id;
        $post_id = $request->input('post_id');
        $comment = $request->input('comment');

        $data = array(
            'comment_id' => random_str(8),
            'post_id' => $post_id,
            'member_id' => $member_id,
            'comment' => $comment,
            'time' => time()
        );

        try {
            DB::beginTransaction();

            $ok = DB::table('post_comments')->insert($data);

            $get = DB::table('member_posts')->join('members', 'member_posts.member_id', '=', 'members.member_id')->where('post_id', $post_id)->first();

            $ok = DB::table('member_posts')->where('post_id', $post_id)->update(['total_comments' => $get->total_comments + 1]);

            DB::commit();

            $responseResult = DB::table('post_comments')->join('users', 'post_comments.member_id', '=', 'users.member_id')->where('post_id', $post_id)->where('post_comments.member_id', $member_id)->first();

            echo json_encode(array(
                'response' => 200,
                'message' => 'success',
                'comment_id' => $responseResult->comment_id,
                'photo' => $responseResult->photo,
                'time' => $responseResult->time
            ));

            // return redirect('/')->with('success', 'Berhasil mengirimkan komentar ke ' . $get->member_name);
        } catch (\Throwable $th) {
            DB::rollBack();
            echo json_encode(array(
                'response' => 501,
                'message' => 'error'
            ));
        }
    }

    public function doDelete(Request $request)
    {
        $key = $request->input('key');
        $context = $request->input('context');

        switch ($context) {
            case 'comment':
                $ok = DB::table('post_comments')
                    ->where('comment_id', $key)
                    ->delete();

                if ($ok) {
                    return redirect('/')->with('success', 'Berhasil menghapus komentar');
                }

                return redirect('/')->with('error', 'Gagal menghapus komentar');

                break;
            case 'post':
                try {
                    DB::beginTransaction();
                    $ok = DB::table('post_images')
                        ->where('post_id', $key)
                        ->delete();

                    $ok = DB::table('member_posts')
                        ->where('post_id', $key)
                        ->delete();

                    $get = DB::table('member_bookmarks')->where('post_id', $key)->get();

                    if ($get) {
                        DB::table('member_bookmarks')->where('post_id', $key)->delete();
                    }

                    DB::commit();
                } catch (\Throwable $th) {
                    DB::rollBack();
                    throw $th;
                }

                if ($ok) {
                    return redirect('/')->with('success', 'Berhasil menghapus postingan');
                }

                return redirect('/')->with('error', 'Gagal menghapus postingan');
            default:
                # code...
                break;
        }
    }

    public function editComment(Request $request)
    {
        $comment_id = $request->input('comment_id');
        $new_comment = $request->input('new_comment');

        $ok = DB::table('post_comments')->where('comment_id', $comment_id)->update(['comment' => $new_comment]);

        if ($ok) {
            return redirect('/')->with('success', 'Berhasil mengedit komentar');
        }

        return redirect('/')->with('error', 'Gagal mengedit komentar');
    }

    public function editCaption(Request $request)
    {
        $post_id = $request->input('post_id');
        $new_caption = $request->input('new_caption');

        $ok = DB::table('member_posts')->where('post_id', $post_id)->update(['caption' => $new_caption]);

        if ($ok) {
            return redirect('/')->with('success', 'Berhasil mengedit caption');
        }

        return redirect('/')->with('error', 'Gagal mengedit caption');
    }

    public function detailPost($params)
    {
        $data['title'] = 'InstaApp | Detail';
        $data['user'] = $this->userDetails;
        $data['post'] = $this->repository->getPostDetail($params)[0];

        return view('home/detail', $data);
    }

    public function doFollowOrUnfollow(Request $request)
    {
        $is_following = $request->input('is_following');
        $member_followed = $request->input('member_followed');
        $member_id = $request->input('member_id');
        $response = array();

        $data = array('member_id' => $member_followed, 'follower_id' => $member_id);

        if($is_following == "true"){
            $ok = DB::table('member_followers')->where('member_id', $member_followed)->where('follower_id', $member_id)->delete();           

            if($ok) {
                $response['response'] = 200;
                $response['is_follow'] = false;
            }
        }else{
            
            $success = DB::table('member_followers')->insert($data);
            
                if($success) {
                    $response['response'] = 200;
                    $response['is_follow'] = true;
                }
        }

        if(empty($response)){
            $response['response'] = 501;
            $response['result'] = json_encode($data);
        }

        echo json_encode($response);
    }


}
