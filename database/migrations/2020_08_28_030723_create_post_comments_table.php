<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_comments', function (Blueprint $table) {
            $table->string('comment_id', 64)->primary();
            $table->string('post_id')->index();
            $table->string('member_id')->index();
            $table->longText('comment')->nullable();
            $table->string('time');
            $table->timestamps();

            $table->foreign('post_id')->references('post_id')->on('member_posts');
            $table->foreign('member_id')->references('member_id')->on('members');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_comments');
    }
}
