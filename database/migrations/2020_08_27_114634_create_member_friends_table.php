<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberFriendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_friends', function (Blueprint $table) {
            $table->id('id');
            $table->string('member_id')->index();
            $table->string('follower_id')->index();
            $table->timestamps();

            $table->foreign('member_id')->references('member_id')->on('members');
            $table->foreign('follower_id')->references('member_id')->on('members');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_friends');
    }
}
