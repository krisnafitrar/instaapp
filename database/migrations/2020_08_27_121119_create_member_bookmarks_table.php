<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberBookmarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_bookmarks', function (Blueprint $table) {
            $table->id();
            $table->string('member_id')->index();
            $table->string('post_id')->index();
            $table->timestamps();

            $table->foreign('member_id')->references('member_id')->on('members');
            $table->foreign('post_id')->references('post_id')->on('member_posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_bookmarks');
    }
}
