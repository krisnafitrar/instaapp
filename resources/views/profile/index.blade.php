@extends('templates.main')
@section('page_title', $title)
@section('content')
<div class="row">
    <div class="col-md-10 mx-auto">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col text-center">
                        <img src="{{ asset('storage/users/' . ($profile->photo == null ? 'default.jpg' : $profile->photo)) }}"
                            alt="" width="150px" height="150px" class="img-fluid img-thumbnail">
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col text-center">
                        <h4><b>{{ $profile->fullname }}</b></h4>
                        <h5>{{ "@" }}{{ $profile->username }}</h5>
                        <div class="col-md-6 mx-auto">
                            <p align="center">
                                {{ $profile->biography ? $profile->biography : 'My Bio' }}
                            </p>
                            <a href="http://{{ $profile->website }}" class="d-block"
                                style="transform: translateY(-10px)">{{ $profile->website }}</a>
                        </div>
                        <div class="col-md-4 mx-auto">
                            <table class="table table-sm table-borderless">
                                <tr>
                                    <td><a
                                            href="{{ url($profile->username . '/followers') }}">
                                            <h4 id="text-followers"
                                                data-id="{{ isset($followers) ? count($followers) : 0 }}">
                                                {{ isset($followers) ? count($followers) : 0 }}</h4>
                                        </a>
                                    </td>
                                    <td>
                                        <a
                                            href="{{ url($profile->username . '/following') }}">
                                            <h4>{{ isset($following) ? count($following) : 0 }}</h4>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Followers</b></td>
                                    <td><b>Following</b></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-3 mx-auto">
                            @if($profile->username != $user->username)
                                @if(!in_array($user->member_id, $followers))
                                    <button data-type="btn-follow" id="btn-follow-{{ $profile->member_id }}"
                                        data-follow="false" data-id="{{ $profile->member_id }}" type="button"
                                        class="btn btn-sm btn-primary btn-block"><i class="fas fa-user mr-1"></i>
                                        Follow</button>
                                @else
                                    <button data-type="btn-follow" id="btn-follow-{{ $profile->member_id }}"
                                        data-follow="true" data-id="{{ $profile->member_id }}" type="button"
                                        class="btn btn-sm btn-secondary btn-block"><i
                                            class="fas fa-user mr-1"></i>Following</button>
                                @endif
                            @endif
                        </div>
                        @if($profile->user_id == $user->user_id)
                            <a href="{{ url($profile->username . '/edit') }}"
                                class="btn btn-sm btn-secondary mt-2"><i class="fas fa-cog mr-1"></i>Edit Profil</a>
                        @endif
                    </div>
                </div>
                <div class="row mt-5">
                    @foreach($posts as $p)
                        @php
                            $images = count($p['images']) > 1 ? $p['images'][0] : $p['images'][0];
                        @endphp
                        <div class="col-md-4">
                            <a
                                href="{{ url($p['post_id'] . '/detail') }}">
                                <img src="{{ asset('storage/posts/' . $images) }}"
                                    class="img-fluid img-thumbnail" width="300px" height="500px">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('[data-type=btn-follow]').click(function () {
        var isFollowing = ($(this).attr('data-follow') == 'true');
        var memberFollowed = $(this).attr('data-id');
        var memberId = "<?= $user->member_id ?>";

        $.ajax({
            type: 'POST',
            url: "<?= url('/follow') ?>",
            data: {
                is_following: isFollowing,
                member_followed: memberFollowed,
                member_id: memberId,
                _token: "{{ csrf_token() }}"
            },
            success: function (response) {
                var res = JSON.parse(response);
                var totalFollowers = parseInt($('#text-followers').attr('data-id'));

                if (res.response == 200) {
                    if (res.is_follow == true) {
                        $('#btn-follow-' + memberFollowed).removeClass('btn btn-primary');
                        $('#btn-follow-' + memberFollowed).addClass('btn btn-secondary');
                        $('#btn-follow-' + memberFollowed).attr('data-follow', 'true');
                        $('#btn-follow-' + memberFollowed).html(
                            '<i class="fas fa-user mr-1"></i>Following');
                        $('#text-followers').attr('data-id', totalFollowers + 1);
                        $('#text-followers').html(totalFollowers + 1);
                    } else {
                        $('#btn-follow-' + memberFollowed).removeClass('btn btn-secondary');
                        $('#btn-follow-' + memberFollowed).addClass('btn btn-primary');
                        $('#btn-follow-' + memberFollowed).attr('data-follow', 'false');
                        $('#btn-follow-' + memberFollowed).html(
                            '<i class="fas fa-user mr-1"></i>Follow');
                        $('#text-followers').attr('data-id', totalFollowers - 1);
                        $('#text-followers').html(totalFollowers - 1);
                    }
                } else {
                    alert('error when request to server, try again!');
                }
            },
            error: function (error) {
                alert('error block, error when request to server, try again!');
            }
        });

    });

</script>
@endsection
