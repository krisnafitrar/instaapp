@extends('templates.main')
@section('page_title', $title)
@section('content')
<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card">
            <div class="card-body">
                <h4> {{ $profile->username }} <b>followers</b></h4>
                <form action="{{ url('search_followers') }}" method="GET" class="mt-5">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="keyword"
                            placeholder="Masukkan username atau email" aria-label="Recipient's username"
                            aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-outline-success" type="button"
                                id="button-addon2">Cari</button>
                        </div>
                    </div>
                </form>
                <div class="mt-3">
                    @forelse($followers as $f)
                        <div class="row mb-2">
                            <div class="col-md-12">
                                <ul class="list-group">
                                    <li class="list-group-item d-flex align-items-center">
                                        <img src="{{ asset('storage/users/' . ($f->photo == null ? 'default.jpg' : $f->photo)) }}"
                                            alt="" width="50px" height="50px" class="img-fluid img-thumbnail mt-2">
                                        <a href="{{ url($f->username . '/user') }}"
                                            class="d-block mt-2">
                                            <span class="ml-3 mr-4"><b>{{ $f->username }}</b></span>
                                        </a>
                                        <div class="col-md-6"></div>
                                        @if($f->member_id != $user->member_id)
                                            @if(!in_array($f->member_id, $following))
                                                <button data-type="btn-follow" id="btn-follow-{{ $f->member_id }}"
                                                    data-follow="false" data-id="{{ $f->member_id }}" type="button"
                                                    class="btn btn-sm btn-primary mt-2"><i class="fas fa-user mr-1"></i>
                                                    Follow</button>
                                            @else
                                                <button data-type="btn-follow" id="btn-follow-{{ $f->member_id }}"
                                                    data-follow="true" data-id="{{ $f->member_id }}" type="button"
                                                    class="btn btn-sm btn-secondary mt-2"><i
                                                        class="fas fa-user mr-1"></i>Following</button>
                                            @endif
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @empty

                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
