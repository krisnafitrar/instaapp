@extends('templates.main')
@section('page_title', $title)
@section('content')
<div class="row">
    <div class="col-3">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab"
                aria-controls="v-pills-home" aria-selected="true">Profil</a>
            <a class="nav-link" id="v-pills-information-tab" data-toggle="pill" href="#v-pills-information" role="tab"
                aria-controls="v-pills-information" aria-selected="true">Informasi Pribadi</a>
            <a class="nav-link" id="v-pills-password-tab" data-toggle="pill" href="#v-pills-password" role="tab"
                aria-controls="v-pills-password" aria-selected="false">Password</a>
        </div>
    </div>
    <div class="col-9">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <div class="card">
                    <div class="card-body">
                        <h4>Edit <b>Profil</b></h4>
                        @php
                            getFlashMessage()
                        @endphp
                        <form action="{{ url('edit-profile') }}" method="POST" class="mt-5"
                            enctype="multipart/form-data">
                            @method('put')
                            @csrf
                            <div class="form-group">
                                <label for="">Nama Lengkap</label>
                                <input type="text" name="fullname" placeholder="Nama Lengkap"
                                    value="{{ $user->fullname }}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" name="username" placeholder="Username"
                                    value="{{ $user->username }}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="">Alamat Email</label>
                                <input type="email" name="email" placeholder="Alamat Email" value="{{ $user->email }}"
                                    class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="">Website</label>
                                <input type="text" name="website" placeholder="Alamat Website"
                                    value="{{ $user->website }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Biografi</label>
                                <textarea name="biography" id="biography" rows="5" placeholder="Biografi Anda"
                                    class="form-control">{{ $user->biography }}</textarea>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupFileAddon01">Foto</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="image" name="image"
                                            aria-describedby="inputGroupFileAddon01" accept="image">
                                        <label class="custom-file-label" for="images">Pilih Foto</label>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="act" value="profile">
                            <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Simpan
                                Perubahan</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="v-pills-information" role="tabpanel"
                aria-labelledby="v-pills-information-tab">
                <div class="card">
                    <div class="card-body">
                        <h4>Informasi <b>Pribadi</b></h4>
                        <form action="{{ url('edit-profile') }}" method="POST" class="mt-5">
                            @method('put')
                            @csrf
                            <div class="form-group">
                                <label for="">Nama Lengkap</label>
                                <input type="text" name="fullname" placeholder="Nama Lengkap"
                                    value="{{ $user->member_name }}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="">Jenis Kelamin</label>
                                <select class="form-control" name="gender" id="gender">
                                    <option value="M"
                                        {{ $user->gender == 'M' ? 'selected' : '' }}>
                                        Laki-Laki</option>
                                    <option value="F"
                                        {{ $user->gender == 'F' ? 'selected' : '' }}>
                                        Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tanggal Lahir</label>
                                <input type="date" name="birthdate" value="{{ $user->birthdate }}"
                                    class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Nomor Telp</label>
                                <input type="number" name="telp_number" value="{{ $user->telp_number }}"
                                    class="form-control" placeholder="No Telepon">
                            </div>
                            <input type="hidden" name="act" value="private">
                            <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Simpan
                                Perubahan</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="v-pills-password" role="tabpanel" aria-labelledby="v-pills-password-tab">
                <div class="card">
                    <div class="card-body">
                        <h4>Ganti <b>Password</b></h4>
                        <form action="{{ url('edit-profile') }}" method="POST" class="mt-5">
                            @method('put')
                            @csrf
                            <div class="form-group">
                                <label for="">Password Lama</label>
                                <input type="password" name="old_password" placeholder="Password Lama"
                                    class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="">Password Baru</label>
                                <input type="password" name="new_password" placeholder="Password Baru"
                                    class="form-control" minlength="8" required>
                            </div>
                            <div class="form-group">
                                <label for="">Ulangi Password</label>
                                <input type="password" name="repeat_password" placeholder="Ulangi Password"
                                    class="form-control" minlength="8" required>
                            </div>
                            <input type="hidden" name="act" value="password">
                            <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Simpan
                                Perubahan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    $('.custom-file-input').on('change', function () {
        var name = $(this)[0].files[0].name;

        var val = $(this).val().toLowerCase(),
            regex = new RegExp("(.*?)\.(jpg|jpeg|png)$");

        if (!(regex.test(val))) {
            $(this).val('');
            alert('Please select correct file format');
        }

        // var fileSize = $(this)[0].files[0].size / 1024;
        // var fileExtension = $(this)[0].files[0].type.split('/')[1];

        // if (fileSize < 2048) {
        //     if (allowedExtension.includes(fileExtension)) {
        //         var name = $(this)[0].files[0].name;
        //         alert(name);
        //     }
        // } else {
        //     alert('Maksimum ukuran file adalah 2Mb!');
        //     $(this).val('');
        // }

        $('.custom-file-label').html(`<b>${ name }</b>`);

    });

</script>
@endsection
