@extends('templates.auth')
@section('page_title', $title)
@section('content')
<h5 class="mb-5 text-center">Masuk <b>InstaApp</b></h5>
@php
    getFlashMessage();
@endphp
<form action="{{ url('/login') }}" method="POST">
    @csrf
    <div class="form-group">
        <label for="identity">Email/Username</label>
        <input type="text" class="form-control @error('identity') is-invalid @enderror" name="identity" id="identity"
            placeholder="Email atau username anda" value="{{ old('identity') }}" required>
        @error('identity')
            <small class="ml-2 text-danger">{{ $message }}</small>
        @enderror
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" name="password" id="password" placeholder="Kata Sandi" required>
    </div>
    <button type="submit" class="btn btn-primary btn-block">Masuk</button>
    <div class="text-center mt-3">
        <a href="{{ url('/registration') }}">Belum punya akun?</a>
    </div>
    <div class="text-center">
        <a href="{{ url('/forgotpassword') }}">Lupa kata sandi?</a>
    </div>
</form>
@endsection
