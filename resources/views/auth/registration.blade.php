@extends('templates.auth')
@section('page_title', $title)
@section('content')
<h5 class="mb-5 text-center">Registrasi <b>InstaApp</b></h5>
@php
    getFlashMessage();
@endphp
<form action="{{ url('/register') }}" method="POST">
    @csrf
    <div class="form-group">
        <label for="fullname">Nama Lengkap</label>
        <input type="text" class="form-control @error('fullname') is-invalid @enderror" name="fullname" id="fullname"
            placeholder="Email atau fullname anda" value="{{ old('fullname') }}" required>
        @error('fullname')
            <small class="ml-2 text-danger">{{ $message }}</small>
        @enderror
    </div>
    <div class="form-group">
        <label for="username">Username</label>
        <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" id="username"
            placeholder="Email atau username anda" value="{{ old('username') }}" required>
        @error('username')
            <small class="ml-2 text-danger">{{ $message }}</small>
        @enderror
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email"
            placeholder="Email atau email anda" value="{{ old('email') }}" required>
        @error('email')
            <small class="ml-2 text-danger">{{ $message }}</small>
        @enderror
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password"
            id="password" placeholder="Kata Sandi" required>
        @error('password')
            <small class="ml-2 text-danger">{{ $message }}</small>
        @enderror
    </div>
    <div class="form-group">
        <label for="password_confirm">Konfirmasi Password</label>
        <input type="password" class="form-control @error('password_confirm') is-invalid @enderror"
            name="password_confirm" id="password_confirm" placeholder="Konfirmasi kata sandi" required>
        @error('password_confirm')
            <small class="ml-2 text-danger">{{ $message }}</small>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary btn-block">Daftar</button>
    <div class="text-center mt-3">
        <a href="{{ url('/') }}">Sudah punya akun?</a>
    </div>
</form>
@endsection
