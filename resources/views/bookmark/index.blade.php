@extends('templates.main')
@section('page_title', $title)
@section('content')
<div class="row">
    <div class="col-md-10 mx-auto">
        <div class="card">
            <div class="card-body">
                <h3>Postingan <b>Disimpan</b></h3>
                <div class="row mt-5">
                    @forelse($bookmarks as $p)
                        <div class="col-md-4">
                            <a href="{{ url($p->post_id . '/detail') }}">
                                <img src="{{ asset('storage/posts/' . $p->image) }}"
                                    class="img-fluid img-thumbnail" width="300px" height="500px">
                            </a>
                        </div>
                    @empty
                        <div class="mx-auto text-secondary">
                            Tidak ada postingan yang disimpan
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
