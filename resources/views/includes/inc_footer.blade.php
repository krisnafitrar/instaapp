<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<div class="modal" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin akan menghapus data ini?</p>
            </div>
            <div class="modal-footer">
                <form action="{{ url('delete') }}" method="POST">
                    @csrf
                    @method('delete')
                    <input type="hidden" name="key" id="delete-key" value="">
                    <input type="hidden" name="context" id="context" value="">
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </form>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Caption</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ url('edit-caption') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <textarea name="new_caption" id="new_caption" class="form-control"
                            placeholder="Ketikkan caption baru kamu .." rows="3"></textarea>
                    </div>
                    <input type="hidden" name="post_id" id="post_id_caption" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    var memberId = "<?= getUserDetails()->member_id ?>";
    var usernameUser = "<?= getUserDetails()->username ?>";
    var allowedExtension = ['jpg', 'jpeg', 'png'];

    $(document).ready(function () {

        $('[data-type=btn-send-comment]').click(function () {
            var postId = $(this).attr('data-id');
            var pageId = $(this).attr('data-page');
            var inputForm = $(`#input-komentar-${ postId }`).val();


            if (inputForm == "" || inputForm == null) {
                alert('Mohon tambahkan komentar!');
            } else {
                $(`#input-komentar-${ postId }`).val('');
                $(this).removeClass('text-primary');
                $(this).addClass('text-secondary');
                $(this).html('<b>Mengirim ..</b>');
                $(this).prop('disabled', true);

                $.ajax({
                    url: "<?= url('comment') ?>",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        comment: inputForm,
                        post_id: postId
                    },
                    success: function (response) {
                        var res = JSON.parse(response);
                        var commentView = ``;

                        if (res.response == 200) {
                            if (pageId != undefined) {
                                var photo = (res.photo != "" || res.photo != null) ? res
                                    .photo : 'default.jpg';

                                commentView += `<img src="<?= asset('storage/users/` +
                                    photo + `  ') ?>"
                                        alt="" width="30px" height="30px" class="img-fluid rounded">
                                        <span class="ml-2 mr-2"><a
                                            href="<?= url(` + usernameUser + ` . '/user') ?>"
                                            style="color: black"><b> ` + usernameUser + `</b></a></span>
                                    <p class="d-inline" style="font-size: 14px;">
                                        ` + inputForm + `
                                    </p>
                                    <small data-type="btn-edit-komentar"
                                        data-id="<?=` + res.comment_id + ` ?>"
                                        class="text-primary ml-1" style="cursor: pointer">Edit</small>
                                    <small data-type="btn-hapus-komentar"
                                        data-id="<?=` + res.comment_id + ` ?>"
                                        class="text-danger ml-1" style="cursor: pointer">Hapus</small>
                                    <div class="form-group"
                                        id="form-edit-komentar-<?=` + res.comment_id + ` ?>">
                                    </div>
                                    <div class="mb-3"></div>`;
                            } else {
                                commentView += `<p><b class="mr-1"><a
                                            href="<?= url('detail/` + usernameUser +
                                    `') ?>"
                                            style="color: black">` + usernameUser + `</a></b>` +
                                    inputForm + `
                                    <?php if(` + memberId + ` == ` + memberId + `): ?>
                                    <small data-type="btn-edit-komentar"
                                        data-id="` + res.comment_id + `"
                                        class="text-primary ml-1" style="cursor: pointer">Edit</small>
                                    <small data-type="btn-hapus-komentar"
                                        data-id="` + res.comment_id + `"
                                        class="text-danger ml-1" style="cursor: pointer">Hapus</small>
                                    <div class="form-group"
                                        id="form-edit-komentar-` + res.comment_id + `">
                                    </div>
                                    <?php endif;?></p>`;
                            }


                            if (pageId != undefined) {
                                $(`.comment-detail-page`).append(commentView);
                            } else {
                                $(`.comment-section-${ postId }`).append(commentView);
                            }

                            $('[data-type=btn-send-comment]').removeClass('text-secondary');
                            $('[data-type=btn-send-comment]').addClass('text-primary');
                            $('[data-type=btn-send-comment]').html('<b>Kirim</b>');
                            $('[data-type=btn-send-comment]').prop('disabled', false);
                        }
                    },
                    error: function (err) {
                        alert('error when sending message!');
                        $('[data-type=btn-send-comment]').removeClass('text-secondary');
                        $('[data-type=btn-send-comment]').addClass('text-primary');
                        $('[data-type=btn-send-comment]').html('<b>Kirim</b>');
                        $('[data-type=btn-send-comment]').prop('disabled', false);
                    }
                });
            }
        });


        $('.caption').on('click', '[data-type=btn-edit-komentar]', function () {
            var commentId = $(this).attr('data-id');
            var baseUrl = "<?= url('') ?>";
            var form = `<form method="post" action="${ baseUrl }/edit-comment">@csrf
                    <div class="row">
                    <div class="col-md-8">
                    <div class="input-group mb-0" style="transform: translateY(-15px)">
                    <input type="text" class="form-control" name="new_comment"
                    placeholder="Komentar baru .."
                    aria-label="Komentar baru"
                    aria-describedby="basic-addon2" style="font-size: 10px;">
                    <input type="hidden" name="comment_id" value="${ commentId }">
                    <div class="input-group-append">
                    <button type="submit" data-type="btn-small-comment" data-id="${ commentId }" class="btn btn-primary" type="button"
                    style="max-height: 29px; font-size: 10px">Perbarui</button>
                    </div>
                    </div>
                    </div>
                    </div>
                    </form>`;

            $('#form-edit-komentar-' + commentId).html(form);
        });

        $('.comment-in-detail').on('click', '[data-type=btn-edit-komentar]', function () {
            var commentId = $(this).attr('data-id');
            var baseUrl = "<?= url('') ?>";
            var form = `<form method="post" action="${ baseUrl }/edit-comment">@csrf
                    <div class="row">
                    <div class="col-md-8">
                    <div class="input-group mb-0" style="transform: translateY(-15px)">
                    <input type="text" class="form-control" name="new_comment"
                    placeholder="Komentar baru .."
                    aria-label="Komentar baru"
                    aria-describedby="basic-addon2" style="font-size: 10px;">
                    <input type="hidden" name="comment_id" value="${ commentId }">
                    <div class="input-group-append">
                    <button type="submit" data-type="btn-small-comment" data-id="${ commentId }" class="btn btn-primary" type="button"
                    style="max-height: 29px; font-size: 10px">Perbarui</button>
                    </div>
                    </div>
                    </div>
                    </div>
                    </form>`;

            $('#form-edit-komentar-' + commentId).html(form);
        });


        $('[data-type=btn-bookmark]').click(function () {
            var postId = $(this).attr('data-id');

            if (postId != null) {
                $.ajax({
                    type: 'POST',
                    url: "<?= url('bookmark') ?>",
                    data: {
                        member_id: memberId,
                        post_id: postId,
                        _token: "{{ csrf_token() }}"
                    },
                    success: function (data) {
                        var obj = JSON.parse(data);

                        if (obj.response == 200) {
                            if (obj.act == "add") {
                                $('#bookmark-icon-' + postId).removeClass('far');
                                $('#bookmark-icon-' + postId).removeClass('fa-bookmark');
                                $('#bookmark-icon-' + postId).addClass('fas fa-bookmark');
                                $('#btn-bookmark-' + postId).removeAttr('style');
                                $('#btn-bookmark-' + postId).attr('style',
                                    'font-size: 24px; color:black;');
                            } else {
                                $('#bookmark-icon-' + postId).removeClass('fas');
                                $('#bookmark-icon-' + postId).removeClass('fa-bookmark');
                                $('#bookmark-icon-' + postId).addClass('far fa-bookmark');
                                $('#btn-bookmark-' + postId).removeAttr('style');
                                $('#btn-bookmark-' + postId).attr('style',
                                    'font-size: 24px;');
                            }
                        }
                    },
                    error: function (err) {
                        alert('error');
                    }
                });
            } else {
                alert('Terjadi error, mohon refresh halaman ini!');
            }
        });


        $('[data-type=btn-edit-caption]').click(function () {
            var postId = $(this).attr('data-id');
            var modal = $('#exampleModalCenter');

            $('#post_id_caption').val(postId);
            modal.modal();
        });


        $('.caption').on('click', '[data-type=btn-hapus-komentar]', function () {
            var modal = $('#modal-delete');
            var key = $(this).attr('data-id');

            $('#delete-key').val(key);
            $('#context').val('comment');

            modal.modal();
        });

        $('.comment-in-detail').on('click', '[data-type=btn-hapus-komentar]', function () {
            var modal = $('#modal-delete');
            var key = $(this).attr('data-id');

            $('#delete-key').val(key);
            $('#context').val('comment');

            modal.modal();
        });


        $('[data-type=btn-hapus-postingan]').click(function () {
            var modal = $('#modal-delete');
            var key = $(this).attr('data-id');

            $('#delete-key').val(key);
            $('#context').val('post');

            modal.modal();
        });


        $('#btn-comment-icon').click(function () {
            var link = $(this).attr('data-id');
            location.href = "<?= url('') ?>" + link + "/comments";
        });


        $('.custom-file-input').on('change', function () {
            var names = [];

            var val = $(this).val().toLowerCase(),
                regex = new RegExp("(.*?)\.(jpg|jpeg|png)$");

            if (!(regex.test(val))) {
                $(this).val('');
                alert('Please select correct file format');
            }

            for (let index = 0; index < $(this).get(0).files.length; index++) {
                var fileSize = $(this).get(0).files[index].size / 1024;
                var fileExtension = $(this).get(0).files[index].type.split('/')[1];

                if (fileSize < 2048) {
                    if (allowedExtension.includes(fileExtension)) {
                        names.push($(this).get(0).files[index].name);
                    }
                } else {
                    alert('Maksimum ukuran file adalah 2Mb!');
                    $(this).val('');
                    break;
                }
            }

            $('.custom-file-label').html(`<b>${ names.join(', ') }</b>`);

        });


        $('#btn-post').click(function () {
            $('#form-post').submit();
        });


        $('[data-type=btn-like]').click(function () {
            var postId = $(this).attr('data-id');

            if (postId != null) {
                $.ajax({
                    type: 'POST',
                    url: "<?= url('like') ?>",
                    data: {
                        member_id: memberId,
                        post_id: postId,
                        _token: "{{ csrf_token() }}"
                    },
                    success: function (data) {
                        var obj = JSON.parse(data);

                        if (obj.response == 200) {
                            if (obj.act == "add") {
                                $('#like-icon-' + postId).removeClass('far');
                                $('#like-icon-' + postId).removeClass('fa-heart');
                                $('#like-icon-' + postId).addClass('fas fa-heart');
                                $('#btn-like-' + postId).removeAttr('style');
                                $('#btn-like-' + postId).attr('style',
                                    'font-size: 24px; color:red;');

                                var total = parseInt($('#total-likes-' + postId).attr(
                                    'data-id')) + 1;

                                $('#total-likes-' + postId).html(total + ' suka');
                                $('#total-likes-' + postId).attr('data-id', total);
                            } else {
                                $('#like-icon-' + postId).removeClass('fas');
                                $('#like-icon-' + postId).removeClass('fa-heart');
                                $('#like-icon-' + postId).addClass('far fa-heart');
                                $('#btn-like-' + postId).removeAttr('style');
                                $('#btn-like-' + postId).attr('style', 'font-size: 24px;');

                                var total = parseInt($('#total-likes-' + postId).attr(
                                    'data-id'));

                                if (parseInt(total) > 0) {
                                    if (total == 1) {
                                        $('#total-likes-' + postId).html(``);
                                        $('#total-likes-' + postId).attr('data-id', 0);
                                    } else {
                                        $('#total-likes-' + postId).html(
                                            `${total - 1} suka`);
                                        $('#total-likes-' + postId).attr('data-id', total -
                                            1);
                                    }
                                } else {
                                    $('#total-likes-' + postId).html(``);
                                    $('#total-likes-' + postId).attr('data-id', 0);
                                }
                            }
                        }
                    },
                    error: function (err) {
                        alert('error');
                    }
                });
            } else {
                alert('Terjadi error, mohon refresh halaman ini!');
            }

        });


        $('#form-post').submit(function (event) {
            event.preventDefault();

            formData = new FormData(this);

            if ($('#images').val()) {
                if ($('#caption').val() == "" || $('#caption').val() == null) {
                    alert('Masukkan Caption!');
                } else {
                    $.ajax({
                        type: 'POST',
                        url: "<?= url('/upload') ?>",
                        data: formData,
                        xhr: function () {
                            var myXHR = $.ajaxSettings.xhr();
                            if (myXHR.upload) {
                                myXHR.upload.addEventListener('progress', progress, false);
                            }
                            return myXHR;
                        },
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (JSON.parse(data).response == 200) {
                                location.href = window.location.href;
                            }
                        },

                        error: function (data) {
                            console.log(JSON.parse(data).message);
                        }
                    });
                }

            } else {
                alert('Harap menyertakan foto!');
            }

        });

        function progress(e) {

            if (e.lengthComputable) {
                var max = e.total;
                var current = e.loaded;

                var Percentage = Math.floor((current * 100) / max);

                progressView = `<label for="" id="label-progress">${Percentage}%</label>
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped progress-percentage progress-bar-animated" role="progressbar"
                                aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: ${Percentage}%"></div>
                        </div>`;

                $('.progress-bar-view').html(progressView);

                $('#btn-post').attr('disabled');
                $('#btn-post').removeClass('btn-success');
                $('#btn-post').addClass('btn-secondary');

                if (Percentage >= 100) {
                    $('#label-progress').html('Finished');
                    $('.progress-percentage').removeClass('progress-bar-animated');
                }
            }
        }

    });

</script>
<script src="{{ asset('assets/js/all.js') }}"></script>
</body>

</html>
