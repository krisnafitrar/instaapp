<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
    <div class="container">
        <a class="navbar-brand ml-3" href="#"><b>InstaApp</b></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>


        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto mr-3">
                <li
                    class="nav-item {{ isset($active) && $active == 'home' ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('/') }}"><i
                            class="fas fa-home mr-1"></i>Home</a>
                </li>
                <li
                    class="nav-item {{ isset($active) && $active == 'search' ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('/search') }}"><i
                            class="fas fa-search mr-1"></i>Search</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ asset('storage/users/' . ($user->photo == null ? 'default.jpg' : $user->photo)) }}"
                            alt="" width="30px" height="30px" class="img-fluid rounded mr-1">
                        {{ $user->username }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item"
                            href="{{ url('/' . $user->username . '/user') }}">Profil</a>
                        <a class="dropdown-item" href="{{ url('/bookmarks') }}">Disimpan</a>
                        <a class="dropdown-item" href="{{ url('logout') }}">Keluar</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
