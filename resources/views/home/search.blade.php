@extends('templates.main')
@section('page_title', $title)
@section('content')
<div class="col col-md-8 mx-auto">
    <div class="card">
        <div class="card-body">
            <h5>Search <b>Pengguna</b></h5>
            <form action="{{ url('search') }}" method="GET" class="mt-3">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" name="keyword" placeholder="Masukkan username atau email"
                        aria-label="Recipient's username" aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-outline-success" type="button"
                            id="button-addon2">Cari</button>
                    </div>
                </div>
            </form>
            @if(isset($_GET['keyword']))
                <span> {{ count($member) }} hasil pencarian dengan keyword
                    "{{ $_GET['keyword'] }}"</span>
            @endif
            <div class="mt-3">
                @if(isset($member))
                    @foreach($member as $m)
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="list-group">
                                    <li class="list-group-item d-flex align-items-center">
                                        <img src="{{ asset('storage/users/' . ($m->photo == null ? 'default.jpg' : $m->photo)) }}"
                                            alt="" width="50px" height="50px" class="img-fluid img-thumbnail mt-2">
                                        <a href="{{ url($m->username . '/user') }}"
                                            class="d-block mt-2">
                                            <span class="ml-3 mr-4"><b>{{ $m->username }}</b></span>
                                        </a>
                                        <div class="col-md-6"></div>
                                        @if($m->member_id != $user->member_id)
                                            @if(!in_array($m->member_id, $member_followers))
                                                <button data-type="btn-follow" id="btn-follow-{{ $m->member_id }}"
                                                    data-follow="false" data-id="{{ $m->member_id }}" type="button"
                                                    class="btn btn-sm btn-primary mt-2"><i class="fas fa-user mr-1"></i>
                                                    Follow</button>
                                            @else
                                                <button data-type="btn-follow" id="btn-follow-{{ $m->member_id }}"
                                                    data-follow="true" data-id="{{ $m->member_id }}" type="button"
                                                    class="btn btn-sm btn-secondary mt-2"><i
                                                        class="fas fa-user mr-1"></i>Following</button>
                                            @endif
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
<script>
    $('[data-type=btn-follow]').click(function () {
        var isFollowing = ($(this).attr('data-follow') == 'true');
        var memberFollowed = $(this).attr('data-id');
        var memberId = "<?= $user->member_id ?>";

        $.ajax({
            type: 'POST',
            url: "<?= url('/follow') ?>",
            data: {
                is_following: isFollowing,
                member_followed: memberFollowed,
                member_id: memberId,
                _token: "{{ csrf_token() }}"
            },
            success: function (response) {
                var res = JSON.parse(response);

                if (res.response == 200) {
                    if (res.is_follow == true) {
                        $('#btn-follow-' + memberFollowed).removeClass('btn btn-primary');
                        $('#btn-follow-' + memberFollowed).addClass('btn btn-secondary');
                        $('#btn-follow-' + memberFollowed).attr('data-follow', 'true');
                        $('#btn-follow-' + memberFollowed).html(
                            '<i class="fas fa-user mr-1"></i>Following');
                    } else {
                        $('#btn-follow-' + memberFollowed).removeClass('btn btn-secondary');
                        $('#btn-follow-' + memberFollowed).addClass('btn btn-primary');
                        $('#btn-follow-' + memberFollowed).attr('data-follow', 'false');
                        $('#btn-follow-' + memberFollowed).html(
                            '<i class="fas fa-user mr-1"></i>Follow');
                    }
                } else {
                    alert('error when request to server, try again!');
                }
            },
            error: function (error) {
                alert('error block, error when request to server, try again!');
            }
        });

    });

</script>
@endsection
