@extends('templates.main')
@section('page_title', $title)
@section('content')
<div class="row mb-5">
    <div class="col-md-9 mx-auto">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        @if(count($post['images']) > 1)
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel"
                                data-interval="false">
                                <div class="carousel-inner">
                                    @php
                                        $index = 0;
                                    @endphp
                                    @foreach($post['images'] as $img)
                                        <div
                                            class="carousel-item {{ $index == 0 ? 'active' : '' }}">
                                            <img src="{{ asset('storage/posts/' . $img) }}"
                                                class="d-block w-100" alt="" style="max-height: 600px" width="100%">
                                        </div>
                                        {{ $index++ }}
                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                    data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                    data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        @else
                            <img src="{{ asset('storage/posts/' . $post['images'][0]) }}"
                                class="img-fluid" alt="" style="max-height: 600px" width="100%">
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="d-none d-lg-block">
                            <img src="{{ asset('storage/users/' . ($post['photo'] ? $post['photo'] : 'default.jpg')) }}"
                                alt="" width="40px" height="40px" class="img-fluid rounded">
                            <span class="ml-2 mr-4"><a
                                    href="{{ url($post['username'] . '/user') }}"
                                    style="color: black"><b>{{ $post['username'] }}</b></a></span>
                            <?php if($user->member_id == $post['member_id']): ?>
                            <div class="dropdown dropleft d-inline d-flex justify-content-end"
                                style="margin-top: -35px;">
                                <button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button data-type="btn-edit-caption"
                                        data-id="{{ $post['post_id'] }}" type="button"
                                        class="dropdown-item">Edit
                                        Caption</button>
                                    <button data-id="{{ $post['post_id'] }}"
                                        data-type="btn-hapus-postingan" type="button"
                                        class="dropdown-item">Hapus</button>
                                </div>
                            </div>
                            <?php endif;?>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="addScroll">
                                <div class="col">
                                    <img src="{{ asset('storage/users/' . ($post['photo'] ? $post['photo'] : 'default.jpg')) }}"
                                        alt="" width="40px" height="40px" class="img-fluid rounded">
                                    <span class="ml-2 mr-2"><a
                                            href="{{ url($post['username'] . '/user') }}"
                                            style="color: black"><b>{{ $post['username'] }}</b></a></span>
                                    <p class="d-inline" style="font-size: 14px;">
                                        {{ $post['caption'] }}
                                    </p>
                                </div>
                                <div class="col mt-4 comment-in-detail">
                                    <div class="comment-detail-page"></div>
                                    <?php if(isset(getComments()[$post['post_id']])): ?>
                                    <?php foreach (getComments()[$post['post_id']] as $p) { ?>
                                    <img src="{{ asset('storage/users/' . ($p['photo'] ? $p['photo'] : 'default.jpg')) }}"
                                        alt="" width="30px" height="30px" class="img-fluid rounded">
                                    <span class="ml-2 mr-2"><a
                                            href="{{ url($p['username'] . '/user') }}"
                                            style="color: black"><b>{{ $p['username'] }}</b></a></span>
                                    <p class="d-inline" style="font-size: 14px;">
                                        {{ $p['comment'] }}
                                    </p>

                                    <?php if($p['member_id'] == $user->member_id): ?>
                                    <small data-type="btn-edit-komentar"
                                        data-id="{{ $p['comment_id'] }}"
                                        class="text-primary ml-1" style="cursor: pointer">Edit</small>
                                    <small data-type="btn-hapus-komentar"
                                        data-id="{{ $p['comment_id'] }}"
                                        class="text-danger ml-1" style="cursor: pointer">Hapus</small>
                                    <div class="form-group mt-4"
                                        id="form-edit-komentar-{{ $p['comment_id'] }}">
                                    </div>
                                    <?php endif;?>
                                    <small class="text-secondary d-block"
                                        style="transform: translateY(-10px)"><?= getDefinitionTime($p['time']) ?></small>
                                    <div class="mb-3"></div>
                                    <?php } ?>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col">
                                @php
                                    $isLikedByUser = isset(getLikes()[$post['post_id']]) ? getLikes()[$post['post_id']]
                                    :
                                    null;
                                @endphp

                                <?php if(isset($isLikedByUser) && in_array($user->member_id, $isLikedByUser)):?>
                                <span class="icon-post mr-3"
                                    id="btn-like-{{ $post['post_id'] }}" data-type="btn-like"
                                    data-id="{{ $post['post_id'] }}"
                                    style="font-size: 24px; color: red;"><i
                                        id="like-icon-{{ $post['post_id'] }}"
                                        class="fas fa-heart"></i></span>
                                <?php else:?>
                                <span class="icon-post mr-3"
                                    id="btn-like-{{ $post['post_id'] }}" data-type="btn-like"
                                    data-id="{{ $post['post_id'] }}" style="font-size: 24px;"><i
                                        id="like-icon-{{ $post['post_id'] }}"
                                        class="far fa-heart"></i></span>
                                <?php endif;?>
                                <a href=#comment_form class="icon-post mr-3" id="btn-comment-icon"
                                    style="font-size: 24px; color: black"><i class="far fa-comment"></i></a>
                                @php
                                    $isBookmarkByUser = isset(getBookmarks()[$post['post_id']]) ?
                                    getBookmarks()[$post['post_id']] : null;
                                @endphp

                                <?php if(isset($isBookmarkByUser) && in_array($user->member_id, $isBookmarkByUser)):?>
                                <span class="icon-post" data-type="btn-bookmark"
                                    data-id="{{ $post['post_id'] }}"
                                    id="btn-bookmark-{{ $post['post_id'] }}"
                                    style="font-size: 24px; color: black;"><i
                                        id="bookmark-icon-{{ $post['post_id'] }}"
                                        class="fas fa-bookmark"></i></span>
                                <?php else:?>
                                <span class="icon-post" data-type="btn-bookmark"
                                    data-id="{{ $post['post_id'] }}"
                                    id="btn-bookmark-{{ $post['post_id'] }}"
                                    style="font-size: 24px"><i
                                        id="bookmark-icon-{{ $post['post_id'] }}"
                                        class="far fa-bookmark"></i></span>
                                <?php endif;?>
                            </div>
                        </div>
                        @if($post['total_likes'] > 0)
                            <span id="total-likes-{{ $post['post_id'] }}"
                                data-id="{{ $post['total_likes'] }}">
                                {{ $post['total_likes'] }} suka</span>
                        @else
                            <span id="total-likes-{{ $post['post_id'] }}"
                                data-id="{{ $post['total_likes'] }}"></span>
                        @endif
                        <div class="row">
                            <div class="col">
                                <small
                                    class="text-secondary">{{ getDefinitionTime($post['time']) }}</small>
                            </div>
                        </div>
                        <hr>
                        <div class="row mt-3">
                            <div class="col">
                                <input type="text" class="custom-input-komentar"
                                    id="input-komentar-{{ $post['post_id'] }}"
                                    placeholder="Tambahkan komentar ..." name="comment" style="" autofocus required>
                                <span class="text-primary" data-type="btn-send-comment"
                                    data-id="{{ $post['post_id'] }}" data-page="detail"
                                    style="font-size: 14px; cursor: pointer"><b>Kirim</b></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
