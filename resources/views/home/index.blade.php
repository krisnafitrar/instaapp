@extends('templates.main')
@section('page_title', $title)
@section('content')
<div class="row">
    <div class="col-md-3 ml-5 d-none d-lg-block">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col text-center">
                        <img src="{{ asset('storage/users/' . ($user->photo == null ? 'default.jpg' : $user->photo)) }}"
                            alt="" width="100px" height="100px" class="img-fluid img-thumbnail">
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col text-center text-black">
                        <a href="{{ url($user->username . '/user') }}"
                            style="color: black"><b>{{ $user->username }}</b></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center text-secondary" style="font-size: 13px">
                        {{ $user->fullname }}
                    </div>
                </div>
            </div>
        </div>
        <div class="card mt-3">
            <div class="card-body">
                <div class="row">
                    <div class="col text-center">
                        <small class="text-secondary">@Copyright
                            {{ date('Y') }} By Krisna Fitra
                            Ramadani</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col col-md-5">
        <div class="card mb-5">
            <div class="card-body">
                <h5 class="mb-3">Bagikan <b>Sesuatu</b></h5>
                @php
                    getFlashMessage();
                @endphp
                <form action="{{ url('upload') }}" method="POST" enctype="multipart/form-data"
                    id="form-post">
                    @csrf
                    <div class="form-group progress-bar-view">
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupFileAddon01"><i
                                        class="fas fa-image"></i></span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="images" name="images[]"
                                    aria-describedby="inputGroupFileAddon01" accept="image" multiple>
                                <label class="custom-file-label" for="images">Pilih Foto</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="caption" id="caption" rows="3" placeholder="Masukkan caption .."
                            class="form-control" autofocus required></textarea>
                    </div>
                    <button type="button" id="btn-post" class="btn btn-success"><i class="fas fa-paper-plane mr-1"></i>
                        Kirim</button>
                </form>
            </div>
        </div>
        <h5><b>Feed</b></h5>
        @foreach($posts as $post)
            <div class="card mb-3">
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <img src="{{ asset('storage/users/' . ($post['photo'] ? $post['photo'] : 'default.jpg')) }}"
                                alt="" width="40px" height="40px" class="img-fluid rounded">
                            <span class="ml-2 mr-4"><a
                                    href="{{ url($post['username'] . '/user') }}"
                                    style="color: black"><b>{{ $post['username'] }}</b></a></span>
                            <?php if($user->member_id == $post['member_id']): ?>
                            <div class="dropdown d-inline d-flex justify-content-end" style="margin-top: -40px">
                                <button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button data-type="btn-edit-caption"
                                        data-id="{{ $post['post_id'] }}" type="button"
                                        class="dropdown-item">Edit
                                        Caption</button>
                                    <button data-id="{{ $post['post_id'] }}"
                                        data-type="btn-hapus-postingan" type="button"
                                        class="dropdown-item">Hapus</button>
                                </div>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                    @if(count($post['images']) > 1)
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel"
                            data-interval="false">
                            <div class="carousel-inner">
                                @php
                                    $index = 0;
                                @endphp
                                @foreach($post['images'] as $img)
                                    <div
                                        class="carousel-item {{ $index == 0 ? 'active' : '' }}">
                                        <img src="{{ asset('storage/posts/' . $img) }}"
                                            class="d-block w-100" alt="" style="max-height: 600px" width="100%">
                                    </div>
                                    {{ $index++ }}
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    @else
                        <img src="{{ asset('storage/posts/' . $post['images'][0]) }}"
                            class="img-fluid" alt="" style="max-height: 600px" width="100%">
                    @endif
                    <div class="row mt-2">
                        <div class="col">
                            @php
                                $isLikedByUser = isset(getLikes()[$post['post_id']]) ? getLikes()[$post['post_id']] :
                                null;
                            @endphp

                            <?php if(isset($isLikedByUser) && in_array($user->member_id, $isLikedByUser)):?>
                            <span class="icon-post mr-3" id="btn-like-{{ $post['post_id'] }}"
                                data-type="btn-like" data-id="{{ $post['post_id'] }}"
                                style="font-size: 24px; color: red;"><i
                                    id="like-icon-{{ $post['post_id'] }}"
                                    class="fas fa-heart"></i></span>
                            <?php else:?>
                            <span class="icon-post mr-3" id="btn-like-{{ $post['post_id'] }}"
                                data-type="btn-like" data-id="{{ $post['post_id'] }}"
                                style="font-size: 24px;"><i
                                    id="like-icon-{{ $post['post_id'] }}"
                                    class="far fa-heart"></i></span>
                            <?php endif;?>
                            <a href=#comment_form class="icon-post mr-3" id="btn-comment-icon"
                                style="font-size: 24px; color: black"><i class="far fa-comment"></i></a>
                            @php
                                $isBookmarkByUser = isset(getBookmarks()[$post['post_id']]) ?
                                getBookmarks()[$post['post_id']] : null;
                            @endphp

                            <?php if(isset($isBookmarkByUser) && in_array($user->member_id, $isBookmarkByUser)):?>
                            <span class="icon-post" data-type="btn-bookmark"
                                data-id="{{ $post['post_id'] }}"
                                id="btn-bookmark-{{ $post['post_id'] }}"
                                style="font-size: 24px; color: black;"><i
                                    id="bookmark-icon-{{ $post['post_id'] }}"
                                    class="fas fa-bookmark"></i></span>
                            <?php else:?>
                            <span class="icon-post" data-type="btn-bookmark"
                                data-id="{{ $post['post_id'] }}"
                                id="btn-bookmark-{{ $post['post_id'] }}"
                                style="font-size: 24px"><i
                                    id="bookmark-icon-{{ $post['post_id'] }}"
                                    class="far fa-bookmark"></i></span>
                            <?php endif;?>
                        </div>
                    </div>
                    @if($post['total_likes'] > 0)
                        <span id="total-likes-{{ $post['post_id'] }}"
                            data-id="{{ $post['total_likes'] }}">
                            {{ $post['total_likes'] }} suka</span>
                    @else
                        <span id="total-likes-{{ $post['post_id'] }}"
                            data-id="{{ $post['total_likes'] }}"></span>
                    @endif
                    <div class="row mt-2">
                        <div class="col caption">
                            <p style="" id="caption-{{ $post['post_id'] }}">
                                <b class="mr-1"><a
                                        href="{{ url($post['username'] . '/user') }}"
                                        style="color: black">{{ $post['username'] }}</a></b>{{ $post['caption'] }}
                            </p>
                            <?php if(isset(getComments()[$post['post_id']]) && count(getComments()[$post['post_id']]) > 5 ):?>
                            <a href="{{ url($post['post_id'] . '/detail') }}"
                                class="d-block" style="transform: translateY(-10px); color: gray">Lihat
                                semua {{ count(getComments()[$post['post_id']]) }}
                                komentar</a>
                            <?php else:?>
                            <?php if(isset(getComments()[$post['post_id']])): ?>
                            <?php foreach (getComments()[$post['post_id']] as $value) { ?>
                            <p style="transform: translateY(-10px)">
                                <b class="mr-1"><a
                                        href="{{ url('detail/' . $value['username']) }}"
                                        style="color: black">{{ $value['username'] }}</a></b>{{ $value['comment'] }}
                                <?php if($value['member_id'] == $user->member_id): ?>
                                <small data-type="btn-edit-komentar"
                                    data-id="{{ $value['comment_id'] }}"
                                    class="text-primary ml-1" style="cursor: pointer">Edit</small>
                                <small data-type="btn-hapus-komentar"
                                    data-id="{{ $value['comment_id'] }}"
                                    class="text-danger ml-1" style="cursor: pointer">Hapus</small>
                                <div class="form-group"
                                    id="form-edit-komentar-{{ $value['comment_id'] }}">
                                </div>
                                <?php endif;?>
                            </p>
                            <?php } ?>
                            <?php endif;?>
                            <?php endif;?>
                            <div class="comment-section-{{ $post['post_id'] }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <small
                                class="text-secondary">{{ getDefinitionTime($post['time']) }}</small>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="custom-input-komentar"
                                id="input-komentar-{{ $post['post_id'] }}"
                                placeholder="Tambahkan komentar ..." name="comment" style="" autofocus required>
                            <span class="text-primary" data-type="btn-send-comment"
                                data-id="{{ $post['post_id'] }}"
                                style="font-size: 14px; cursor: pointer"><b>Kirim</b></span>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
