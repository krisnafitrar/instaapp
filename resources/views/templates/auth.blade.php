@include('includes.inc_header')

<div class="container">
    <div class="row">
        <div class="col-md-5 mx-auto mt-5">
            <div class="card">
                <div class="card-body">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>

</script>
<script src="{{ asset('assets/js/all.js') }}"></script>
</body>

</html>
