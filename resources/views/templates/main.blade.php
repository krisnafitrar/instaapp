@include('includes.inc_header')
<!-- navbar section -->
@include('includes.inc_navbar')
<!-- end of navbar-->

<section class="main-content">
    <div class="container-fluid">
        @yield('content')
    </div>
</section>

@include('includes.inc_footer')
